import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterComponent }  from '@component/register/register.component';
import { LoginComponent }     from '@component/login/login.component';
import { HomeComponent }      from '@component/home/home.component';
import { PostComponent }      from '@component/post/post.component';
import { NewPostComponent }   from '@component/new-post/new-post.component';
import { ShowPostComponent }  from '@component/show-post/show-post.component';
import { EditPostComponent }  from '@component/edit-post/edit-post.component';
const routes: Routes = [
  { path: 'register', component: RegisterComponent},
  { path: 'login', component: LoginComponent },
  { path: 'posts', component: PostComponent},
  { path: 'post/new', component: NewPostComponent },
  { path: 'post/:id', component: ShowPostComponent },
  { path: 'post/:id/edit', component: EditPostComponent},
  { path: '**', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
