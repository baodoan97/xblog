import { Component, OnInit } from '@angular/core';
import { User }              from '@model/user';
import { UserService}        from '@service/user.service';
import { Router }            from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: User = new User();
  constructor(
    private userSerivce: UserService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  logIn() {
    this.userSerivce.logIn(this.user);
    this.router.navigateByUrl('');
  }

}
