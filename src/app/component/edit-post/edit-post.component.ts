import { Component, OnInit }          from '@angular/core';
import { Post }                       from '@model/post';
import { PostService }                from '@service/post.service';
import { ActivatedRoute, Router }     from '@angular/router';
import * as ClassicEditor             from '@ckeditor/ckeditor5-build-classic';
@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.css']
})
export class EditPostComponent implements OnInit {
  post = new Post();
  public Editor = ClassicEditor;
  constructor(
    private postService: PostService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.getPost();
  }

  getPost(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.post = this.postService.getPost(id);
  }

  editPost() {
    this.postService.editPost(this.post);
    this.router.navigate(['/post', this.post.id]);
  }
}
