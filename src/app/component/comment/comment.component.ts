import { Component, OnInit, Input }     from '@angular/core';
import { Comment }                      from '@model/comment';
import { CommentService }               from '@service/comment.service';
import { Post }                         from '@model/post';
import { UserService }                  from '@service/user.service';
import { Router }                       from '@angular/router';
@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {
  comment = new Comment();
  comments;
  user;

  @Input() post: Post;
  constructor(
    private commentService: CommentService,
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit() {
    this.comments = this.commentService.getComments(this.post.id);
    this.user = this.userService.getCurrentUser();
  }

  addComment() {
    this.commentService.addComment(this.post.id, this.comment, this.user.id);
    this.comments = this.commentService.getComments(this.post.id);
    this.router.navigate(['/post', this.post.id]);
  }
}
