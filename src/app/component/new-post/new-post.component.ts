import { Component, OnInit }      from '@angular/core';
import { Post }                   from '@model/post';
import { PostService }            from '@service/post.service';
import { UserService }            from '@service/user.service';
import * as ClassicEditor         from '@ckeditor/ckeditor5-build-classic';
import { Router }                 from '@angular/router';
@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.css']
})
export class NewPostComponent implements OnInit {
  post: Post = new Post();
  public Editor = ClassicEditor;
  constructor(
    private postService: PostService,
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  addPost() {
    this.postService.addPost(this.post, this.userService.getCurrentUser().id);
    this.router.navigateByUrl('');
  }

}


