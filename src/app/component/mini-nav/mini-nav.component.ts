import { Component, OnInit }        from '@angular/core';
import { UserService }              from '@service/user.service';
import { User }                     from '@model/user';

@Component({
  selector: 'app-mini-nav',
  templateUrl: './mini-nav.component.html',
  styleUrls: ['./mini-nav.component.css']
})
export class MiniNavComponent implements OnInit {
  user: User;
  constructor(
    private userService: UserService,
  ) { }

  ngOnInit() {
    this.user = this.userService.getCurrentUser();
  }

  logout() {
    this.userService.logOut();
  }
}
