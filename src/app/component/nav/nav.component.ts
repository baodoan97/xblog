import { Component, OnInit }        from '@angular/core';
import { UserService }              from '@service/user.service';
import { User }                     from '@model/user';
@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  user: User;
  constructor(
    private userService: UserService,
  ) { }

  ngOnInit() {
    this.user = this.userService.getCurrentUser();
  }

  logout() {
    this.userService.logOut();
  }
}
