import { Component, OnInit }      from '@angular/core';
import { ActivatedRoute }         from '@angular/router';

import { Post }                   from '@model/post';
import { PostService }            from '@service/post.service';

@Component({
  selector: 'app-show-post',
  templateUrl: './show-post.component.html',
  styleUrls: ['./show-post.component.css']
})
export class ShowPostComponent implements OnInit {
  post: Post;
  user;

  constructor(
    private postService: PostService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
   this.getPost();
  }

  getPost(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.post = this.postService.getPost(id);
    let users =  JSON.parse(localStorage.getItem('users'));
    this.user = users.find(x => x.id = this.post.id);
  }
}
