import { Component, OnInit } from '@angular/core';
import { Post }              from '@model/post';
import { PostService }       from '@service/post.service';
@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  posts: Post[];
  constructor(
    private postService: PostService
  ) { }

  ngOnInit() {
   this.getPosts();
  }

  getPosts(): void {
    this.postService.getPosts().subscribe(posts => this.posts = posts);
  }

}
