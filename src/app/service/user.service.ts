import { Injectable }     from '@angular/core';
import { User }           from '@model/user';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  user: User;
  constructor() { }
  users = JSON.parse(localStorage.getItem('users')) || [];
  
  addUser(user: User) {
    let users = JSON.parse(localStorage.getItem('users')) || [];
    user.id = users.length ? Math.max(...users.map(x => x.id)) + 1 : 1;
    users.push(user);
    localStorage.setItem('users', JSON.stringify(users));
  }

  logOut() {
    localStorage.removeItem('currentUser');
    this.user = null;
  }

  logIn(user: User) {
    let users = JSON.parse(localStorage.getItem('users')) || [];
    this.user = users.find( x => x.email === user.email && x.password === user.password);
    if(this.user){
      localStorage.setItem('currentUser', JSON.stringify(this.user));
    }

  }

  getCurrentUser() {
    // if(this.user){
    //   return this.user;
    // }
    if (localStorage.getItem('currentUser')) {
      return this.user = JSON.parse(localStorage.getItem('currentUser'));
    }
  }
}
