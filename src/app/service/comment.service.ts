import { Injectable }       from '@angular/core';
import { Comment }          from '@model/comment';
import { UserService }      from '@service/user.service';
@Injectable({
  providedIn: 'root'
})
export class CommentService {
  comments = JSON.parse(localStorage.getItem('comments')) || [];
  constructor(
    private userService: UserService
  ) { }

  addComment(post_id: number, comment: Comment, user_id: number) {
    comment.id = this.comments.length ? Math.max(...this.comments.map(x => x.id)) + 1 : 1;
    comment.post_id = post_id;
    comment.user_id = user_id;
    this.comments.push(comment);
    localStorage.setItem('comments', JSON.stringify(this.comments));
  }

  getComments(post_id: number): any {
    let result: Array<{id: number, content: string, email: string}> = [];
    this.comments.forEach(x =>  {
                                  // tslint:disable-next-line: curly
                                  if (x.post_id === post_id){
                                    result.push({
                                      id: x.id,
                                      content: x.content,
                                      email: this.userService.users.find((user: { id: any; }) => user.id === x.user_id).email
                                    });
                                      // tslint:disable-next-line: no-trailing-whitespace
                                  }
                                });
    return result;
  }
}
