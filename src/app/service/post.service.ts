import { Injectable }     from '@angular/core';
import { Post }           from '@model/post';
import { Observable, of } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class PostService {
  posts = JSON.parse(localStorage.getItem('posts')) || [];
  post: Post;
  constructor() { }

  getPosts(): Observable<Post[]> {
    return of(this.posts);
  }

  addPost(post: Post, user_id: number) {
    this.post = post;
    this.post.user_id = user_id;
    this.post.id = this.posts.length ? Math.max(...this.posts.map(x => x.id)) + 1 : 1;
    this.posts.push(this.post);
    localStorage.setItem('posts', JSON.stringify(this.posts));
  }

  getPost(id: number) {
    return this.posts.find(x => x.id === id);
  }

  editPost(post: Post) {
    let posts = JSON.parse(localStorage.getItem('posts')) || [];
    this.post = post;
    posts.forEach(x =>  {
      // tslint:disable-next-line: curly
      if (x.id === this.post.id){
          x.title = this.post.title;
          x.content= this.post.content;
          // tslint:disable-next-line: no-trailing-whitespace
      }
    });
    localStorage.setItem('posts', JSON.stringify(posts));
  }
}
