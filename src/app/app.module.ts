import { BrowserModule }          from '@angular/platform-browser';
import { NgModule }               from '@angular/core';
import { FormsModule }            from '@angular/forms';
import { CKEditorModule }         from '@ckeditor/ckeditor5-angular';

import { AppRoutingModule }       from './app-routing.module';
import { AppComponent }           from './app.component';
import { NavComponent }           from '@component/nav/nav.component';
import { RegisterComponent }      from '@component/register/register.component';
import { LoginComponent }         from '@component/login/login.component';
import { FooterComponent }        from '@component/footer/footer.component';
import { HomeComponent }          from '@component/home/home.component';
import { PostComponent }          from '@component/post/post.component';
import { NewPostComponent }       from '@component/new-post/new-post.component';
import { ShowPostComponent }      from '@component/show-post/show-post.component';
import { CommentComponent }       from '@component/comment/comment.component';
import { EditPostComponent }      from '@component/edit-post/edit-post.component';
import { MiniNavComponent }       from '@component/mini-nav/mini-nav.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    RegisterComponent,
    LoginComponent,
    FooterComponent,
    HomeComponent,
    PostComponent,
    NewPostComponent,
    ShowPostComponent,
    CommentComponent,
    EditPostComponent,
    MiniNavComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    CKEditorModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
