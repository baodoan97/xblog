export class Comment {
    id: number;
    content: string;
    user_id: number;
    post_id: number;
}